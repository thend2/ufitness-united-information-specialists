package edu.uis.ufitness_gui;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Database_Methods {

	private final static  String URL = "jdbc:oracle:thin:thend2/csc478CTeam4@oracle.uis.edu:1521:oracle";
		
	private static Connection conn = null;
	private static PreparedStatement stmt = null;
        
        
        private static Food food = new Food();
        private static String SQL, formatedDate;
        private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        private static ArrayList<Food> foodList = new ArrayList<Food>();


	public static void createConnection() {

		// Connect to the oracle database
		try {
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			conn = DriverManager.getConnection(URL);
			if (conn != null) {
				System.out.println("Connected");
			}
		} catch (SQLException e) {
			System.out.println("Exception occured while creating the database connection!");
			JOptionPane.showMessageDialog(null, "Connection could not be established to the database!");
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void closeConnection() {
		// Close the connection to the database
		try {
			if (conn != null && !conn.isClosed()) {
				//conn.commit();
				conn.close();
				if (conn.isClosed()) {
					System.out.println("Connection Closed");
				}
			}
		} catch (SQLException e) {
			System.out.println("Exception occured while closing the database connection!");
			e.printStackTrace();
		}
	}

	public static void closePreparedStatement() {
		// Close the prepared statement
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				System.out.println("Exception occured while closing the prepared statement!");
				e.printStackTrace();
			}
		}
	}

	//Requirement 1.1.2 User is taken to the profile tab of the profile window upon logging in
	public static void login(String userName, String password) {
		String storedPassword = null;
		createConnection();
		ResultSet rs = null;
		String sql = "SELECT PASSWORD FROM PERSON WHERE USERNAME = ? ";
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			rs = stmt.executeQuery();
			rs.next();
			storedPassword = rs.getString("PASSWORD"); //Find what password is stored for the username
			if (password.compareTo(storedPassword) == 0) { //If passwords match, log into application
				initiateUser(userName);

				ProfileWindow prof = new ProfileWindow();
			      prof.setVisible(true);
			        try {
			            TimeUnit.MILLISECONDS.sleep(250);
			        } catch (InterruptedException ex) {
			            Logger.getLogger(LoginWindow.class.getName()).log(Level.SEVERE, null, ex);
			        }			        		       
			        SplashWindow.login.close();
			       
			        System.out.println("closed");			        
			} 
			//if the passwords do not match, popup and error message 
			else {System.out.println("an incorrect username or password has been entered"); //If passwords don't match, display error message
			JOptionPane.showMessageDialog(null, "ALERT! - Wrong Username or Password!"
					+ "\n Please Try Again, or Create New Account");
			}
		}
			 
		catch (SQLException e) {
			//if an exception occues, popup error message
				System.out.println("an incorrect username or password has been added"); //If error, username does not exist, display error message
				JOptionPane.showMessageDialog(null, "ALERT! - Wrong Username or Password!"
						+ "\n Please Try Again, or Create New Account");
		}
		finally{
			closePreparedStatement();
			closeConnection();
		}
	}

	//Requirement 1.1.1 Create new account
	public static boolean createAccount(String name, String userName, String password,
									 int weight, int height, char gender, Calendar DOB, byte[] image) {

		java.sql.Date sqlDate = new java.sql.Date(DOB.getTime().getTime()); //Convert java.util date to java.sql date

		createConnection();
		ResultSet rs = null;
		String sql = "SELECT USERNAME FROM PERSON WHERE USERNAME = ? ";  //Check to see if username already exists
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			rs = stmt.executeQuery();
			if (rs.next()){
				System.out.println("Username already exists, please choose a different username");
				JOptionPane.showMessageDialog(null, "Username already exists, please choose a different username.");
				return false;
			}
			else { //if username does not already exist, create a new account with the given attributes
				sql = "INSERT INTO PERSON VALUES(PersonID_Seq.nextval, ?, ?, ?, ?, ?, ?, ?, ? )";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, userName);
				stmt.setString(2, password);
				stmt.setString(3, name);
				stmt.setString(4, Character.toString(gender));
				stmt.setDate(5, sqlDate);
				stmt.setInt(6, height);
				stmt.setInt(7, weight);
				stmt.setBytes(8, image);
				rs = stmt.executeQuery();
				initiateUser(userName);
			}

		} catch (SQLException e) {
			System.out.println("error");
			e.printStackTrace();
		} finally {
			closePreparedStatement();
			closeConnection();
		}
		return true;
	}

	//Requirement 1.2.2.1 This method allows users to edit and save profile information
	public static boolean editAttributes(String name, String userName, String password,
									  int weight, int height, char gender, Calendar DOB, byte[] image) {

		java.sql.Date sqlDate = new java.sql.Date(DOB.getTime().getTime()); //Convert java.util date to java.sql date

		//If user is trying to change username, check to make sure the new username does not already exist in database.
		createConnection();
		if(userName.compareTo(UfItnesS_GUI_Driver.currentUser.getUserName()) != 0){
			try{
				ResultSet rs = null;
				String sql = "SELECT USERNAME FROM PERSON WHERE USERNAME = ? ";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, userName);
				rs = stmt.executeQuery();
				if (rs.next())
					throw new SQLException();
			} catch (SQLException e) {
				System.out.println("Username already exists, please choose a different username");
				JOptionPane.showMessageDialog(null, "Username already exists, please choose a different username.");
				e.printStackTrace();
				return false;
			}
		}
		//Update the database with the entered changes
		try {
			String sql = "UPDATE PERSON SET username = ?, password = ?, name = ?, gender = ?, birth_date = ?,"
					+ " height = ?, weight = ?, image = ? WHERE username = ? ";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			stmt.setString(2, password);
			stmt.setString(3, name);
			stmt.setString(4, Character.toString(gender));
			stmt.setDate(5, sqlDate);
			stmt.setInt(6, height);
			stmt.setInt(7, weight);
			stmt.setBytes(8, image);
			stmt.setString(9, UfItnesS_GUI_Driver.currentUser.getUserName());
			stmt.executeUpdate();
			System.out.println("executed query");
			initiateUser(userName);

		} catch (SQLException e) {
			System.out.println("error editing profile");
			e.printStackTrace();
		} finally {
			closePreparedStatement();
			closeConnection();
		}
		return true;
	}

	public static void initiateUser(String userName) {
		//Sets the current user to the selected username to be used for pulling
		//data about the current user. 
		
		createConnection();
		ResultSet rs = null;

		String sql = "SELECT * FROM PERSON WHERE USERNAME = ? ";
		try {
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			rs = stmt.executeQuery();
			rs.next();

			Calendar cal = new GregorianCalendar();
			cal.setTime(rs.getDate("BIRTH_DATE"));

			UfItnesS_GUI_Driver.currentUser = new Person(rs.getString("name"), rs.getString("username"),
					rs.getString("password"), rs.getInt("weight"), rs.getInt("height"), rs.getString("gender").charAt(0),
					cal, getImage(rs.getBlob("image")), rs.getInt("personID"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void fillExerciseTable(DefaultTableModel exerciseTable, String date){
		java.sql.Date sqlDate = convertDate(date, "MM/dd/yyyy");
		ResultSet rs = null;
		createConnection();
		//fill in the exercise table from the database
		try{
			String sql = "SELECT ExerciseID, Name, ExerciseDate, Duration, MPH, Calories, Favorite FROM ExerciseEntry "
                    + "where PersonID = ? and ExerciseDate LIKE ? || '%' ORDER BY ExerciseDate";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1,  UfItnesS_GUI_Driver.currentUser.getPersonID());
			stmt.setDate(2, sqlDate);

			rs = stmt.executeQuery();

			for (int row = 0; rs.next(); row++){
				exerciseTable.insertRow(row, new Object[]{rs.getInt("ExerciseID"), rs.getDate("ExerciseDate"),
                        rs.getString("Name"), rs.getDouble("MPH"), rs.getInt("Calories"), rs.getBoolean("Favorite")});
			}
			System.out.println("Exercise table filled");
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			closePreparedStatement();
			closeConnection();
		}
	}

	//Requirement 1.4.2.1.1.4 Adds to exercise entry to the database
	public static void addExercise(String exerciseDate, String name, long calories, int duration, double mph, Boolean favorite) {
        java.sql.Date sqlDate = convertDate(exerciseDate, "MM/dd/yyyy hh:mm:ss a");
		createConnection();
		ResultSet rs = null;
		int personID = UfItnesS_GUI_Driver.currentUser.getPersonID();

		try {
			String sql = "INSERT INTO ExerciseEntry VALUES (ExerciseID_Seq.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, personID);
			stmt.setObject(2, sqlDate);
			stmt.setString(3, name);
			stmt.setLong(4, calories);
			stmt.setInt(5, duration);
			stmt.setDouble(6, mph);
			stmt.setBoolean(7, favorite);
			rs = stmt.executeQuery();
			rs.next();

			System.out.println("Exercise added successfully");

		} catch (SQLException e) {
			e.printStackTrace();

		}
		try {
			String sql = "SELECT ExerciseID from ExerciseEntry WHERE ExerciseDate = ? AND Name = ? AND Calories = ?";

			stmt = conn.prepareStatement(sql);

			stmt.setDate(1, sqlDate);
			stmt.setString(2, ExerciseWindow.currentExercise.getName());
			stmt.setLong(3, ExerciseWindow.currentExercise.getCaloriesBurned());

			rs = stmt.executeQuery();
			rs.next();
			System.out.println("Executed Query");

			ExerciseWindow.currentExercise.setExerciseID(rs.getInt("ExerciseID"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closePreparedStatement();
			// closeConnection();
		}
	}
	
	//Requirement 1.4.2.1.1.1.4.1 Add exercise to the current day's list
	public static DefaultListModel fillExerciseList (DefaultListModel exerciseList) {
		createConnection();
		ResultSet rs = null;

		String sql = "SELECT ExerciseID, Name, Calories, MPH, Duration, ExerciseDate FROM ExerciseEntry " +
                "WHERE PersonID = ? ORDER BY ExerciseDate DESC";

		try {
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());

			rs = stmt.executeQuery();

            while (rs.next()) {
                exerciseList.add(0, rs.getInt("ExerciseID") + "    " + rs.getString("Name") + "    " +
                        rs.getDouble("MPH") + "    " + rs.getInt("Duration") + "    " + rs.getInt("Calories") +
                        "    " + rs.getDate("ExerciseDate"));
            }
            System.out.println("All exercises list filled successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closePreparedStatement();
			//closeConnection();
		}
		return exerciseList;
	}

	//Requirement 1.4.2.1.1.4.1 Adds exercise entry to recent list
	public static DefaultListModel fillRecentExerciseList (DefaultListModel recentExercises) {
        createConnection();
        ResultSet rs = null;

        String sql = "SELECT ExerciseID, Name, Calories, MPH, Duration, ExerciseDate FROM ExerciseEntry " +
                "WHERE PersonID = ? ORDER BY ExerciseDate DESC";

        try {
            stmt = conn.prepareStatement(sql);

            stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());

            rs = stmt.executeQuery();
            System.out.println("Executed Query");

            for (int i = 0; rs.next() && i < 5; i++) {
                recentExercises.addElement(rs.getInt("ExerciseID") + "    " + rs.getString("Name") + "    " +
                        rs.getDouble("MPH") + "    " + rs.getInt("Duration") + "    " + rs.getInt("Calories") +
                        "    " + rs.getDate("ExerciseDate"));
            }
            System.out.println("Recent exercise list filled successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
            //closeConnection();
        }
        return recentExercises;
    }
	//Requirement 1.4.2.2.1 Deletes activity from database
	public static void deleteExercise(Object exercise) {
		createConnection();
		ResultSet rs = null;

		String sql = "DELETE FROM ExerciseEntry WHERE ExerciseID = ?";
        String exerciseStr = exercise.toString();
        String delim = "[ ]+";
        String[] tokens = exerciseStr.split(delim);
        int exerciseID = Integer.parseInt(tokens[0]);

		try {
			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, exerciseID);

            rs = stmt.executeQuery();
            rs.next();

			System.out.println("Exercise deleted successfully");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closePreparedStatement();
			closeConnection();
		}

	}

	//Requirement 1.4.2.2.3 Adds favorite activity to favorites list
	public static DefaultListModel fillFavoriteExerciseList (DefaultListModel favoriteExercises) {
	    createConnection();
        ResultSet rs = null;

        String sql = "SELECT ExerciseID, Name, Calories, MPH, Duration, ExerciseDate FROM ExerciseEntry WHERE " +
                "Favorite = '1' AND PersonID = ?";

        try {
            stmt = conn.prepareStatement(sql);

            stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());

            rs = stmt.executeQuery();

            while (rs.next()) {
                favoriteExercises.add(0, rs.getInt("ExerciseID") + "    " + rs.getString("Name") + "    " +
                        rs.getDouble("MPH") + "    " + rs.getInt("Duration") + "    " + rs.getInt("Calories") +
                        "    " + rs.getDate("ExerciseDate"));
            }
            System.out.println("Favorite exercise list filled successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
            //closeConnection();
        }
        return favoriteExercises;
    }

	//Requirement 1.4.2.1.1.2.1 User Can delete a favorite exercise
    public static void deleteFavoriteExercise (Object exercise) {
        createConnection();
        ResultSet rs = null;

        String sql = "UPDATE ExerciseEntry SET Favorite = '0' WHERE ExerciseID = ?";

        String exerciseStr = exercise.toString();
        String delim = "[ ]+";
        String[] tokens = exerciseStr.split(delim);
        int exerciseID = Integer.parseInt(tokens[0]);

        try {
            stmt = conn.prepareStatement(sql);

            stmt.setInt(1, exerciseID);

            rs = stmt.executeQuery();
            rs.next();
            System.out.println("Favorite exercise deleted successfully");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement();
            closeConnection();
        }
    }
    
    //Requirement 1.2.1 This method converts database data to create a user's profile image
    private static BufferedImage getImage(Blob b) {
    	BufferedImage bImage = null;
    	
    	// Retrieve image if one exists
    	if (b != null) {
    		try {
				byte[] bArr = b.getBytes(1, (int) b.length()); // Get bytes from Blob
				InputStream input = new ByteArrayInputStream(bArr);
				bImage = ImageIO.read(input); // Read byte stream as an image
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	
    	return bImage;
    }
    
    // Food Database methods
    
    //Requirement 1.3.3.1.1.2.2 User can delete a favorite food. This method changes a food 
    //in the database from a favorite to a nonfavorite.
    public static void deleteFavoriteFood (Object food) {
        createConnection();
        ResultSet rs = null;
        String sql = "UPDATE FoodEntry SET Favorite = '0' WHERE FoodID = ?";
        String foodStr = food.toString();
        String delim = "[ ]+";
        String[] tokens = foodStr.split(delim);
        int foodID = Integer.parseInt(tokens[0]);
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, foodID);
            rs = stmt.executeQuery();
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        closePreparedStatement();
        closeConnection();
    }
    
    //Requirement 1.3.3.1.1.2
    //This  method pulls all foods that have been marked as a favorite
    public static DefaultListModel fillFavoriteFoodList (DefaultListModel favoriteFoods) {
        createConnection();
        ResultSet rs = null;
        String sql = "SELECT FoodID, Name, Calories, FoodDate, Favorite FROM FoodEntry WHERE Favorite = '1' AND PersonID = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                favoriteFoods.add(0, rs.getInt("FoodID") + "    " + rs.getString("Name") + "    " +
                        rs.getInt("Calories") + "    " + rs.getDate("FoodDate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
        return favoriteFoods;
    }
    
    //Requirement 1.3.3.1.1 The list of food for the day will be displayed on the food entry page
    //This method pulls the food data from the database to populate the "current" list on the food entry page
    public static DefaultListModel fillFoodList (DefaultListModel foodList) {
        createConnection();
        ResultSet rs = null;
        String sql = "SELECT FoodID, Name, Calories, FoodDate FROM FoodEntry WHERE PersonID = ? ORDER BY FoodDate DESC";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());
            rs = stmt.executeQuery();
            while (rs.next()) {
                foodList.add(0, rs.getInt("FoodID") + "    " + rs.getString("Name") + "    " +
                        rs.getInt("Calories") + "    " + rs.getDate("FoodDate"));
            }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        closePreparedStatement();
        return foodList;
    }
    
  //Requirement 1.3.1 A table with food data will be displayed on the food tab
  //This method pulls food data from the database and adds it to the table
    public static void fillFoodTable(DefaultTableModel foodTable, String date){
        java.sql.Date sqlDate = convertDate(date, "MM/dd/yyyy");
        ResultSet rs = null;
        createConnection();
        try{
            String sql = "SELECT FoodID, Name, FoodDate, Calories, Servings, Favorite FROM FoodEntry "
                + "where PersonID = ? and FoodDate LIKE ? || '%' ORDER BY FoodDate";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1,  UfItnesS_GUI_Driver.currentUser.getPersonID());
            stmt.setDate(2, sqlDate);
            rs = stmt.executeQuery();
            for (int row = 0; rs.next(); row++){
                //System.out.println(rs.getBoolean("Favorite"));
                foodTable.insertRow(row, new Object[]{rs.getInt("FoodID"), rs.getDate("FoodDate"),
                		rs.getString("Name"), rs.getInt("Calories"), rs.getInt("Servings"), rs.getBoolean("Favorite")});
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            closePreparedStatement();
            closeConnection();
        }
    }

	//This method converts dates from a SimpleDateFormat to a format recognized by sql
    private static java.sql.Date convertDate(String date, String dateFormat) {
		SimpleDateFormat curFormatter = new SimpleDateFormat(dateFormat);
        Date dateObj = null;
        try {
            dateObj = curFormatter.parse(date); //convert formatted date to date object
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateObj); //convert date object to calendar object
        java.sql.Date sqlDate = new java.sql.Date(calendar.getTime().getTime()); //Convert java.util date to java.sql date
		return sqlDate;
	}

    //Requirement 1.3.3.1.1.1 User can add a new food item
    //This method enters new food data into the database, based on what was entered on the food
    //entry page.
    public static void addFood(String foodDate, String name, long calories, int servings, Boolean favorite) {
        java.sql.Date sqlDate = convertDate(foodDate, "MM/dd/yyyy hh:mm:ss a");
        createConnection();
        ResultSet rs = null;
        int personID = UfItnesS_GUI_Driver.currentUser.getPersonID();
        try {
            String sql = "INSERT INTO FoodEntry VALUES (FoodID_Seq.NEXTVAL, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, personID);
            stmt.setObject(2, sqlDate);
            stmt.setString(3, name);
            stmt.setLong(4, calories);
            stmt.setInt(5, servings);
            stmt.setBoolean(6, favorite);
            rs = stmt.executeQuery();
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            String sql = "SELECT FoodID from FoodEntry WHERE FoodDate = ? AND Name = ? AND Calories = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setDate(1, sqlDate);
            stmt.setString(2, FoodWindow.currentFood.getName());
            stmt.setLong(3, FoodWindow.currentFood.getCalories());
            rs = stmt.executeQuery();
            rs.next();
            System.out.println("Executed Query");
            FoodWindow.currentFood.setFoodID(rs.getInt("FoodID"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
    }
	
    //Requirement 1.3.3.1.1.3 Recent foods are displayed in the food entry window
    //THis method pulls the 5 most recent foods from the database
    public static DefaultListModel fillRecentFoodList (DefaultListModel recentFoods) {
        createConnection();
        ResultSet rs = null;
        String sql = "SELECT FoodID, Name, Calories, FoodDate FROM FoodEntry WHERE PersonID = ? "
        		+ "ORDER BY FoodDate DESC";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, UfItnesS_GUI_Driver.currentUser.getPersonID());
            rs = stmt.executeQuery();
            System.out.println("Executed Query");
            for (int i = 0; rs.next() && i < 5; i++) {
                recentFoods.addElement(rs.getInt("FoodID") + "    " + rs.getString("Name") + "    " +
                		rs.getInt("Calories") + "    " + rs.getDate("FoodDate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
        return recentFoods;
    }

    //Requirement 1.3.3.1.2 The User has the option to delete each food entry
    //This method deletes a food entry from the database
    public static void deleteFood(Object food) {
        createConnection();
        ResultSet rs = null;
        String sql = "DELETE FROM FoodEntry WHERE FoodID = ?";
        String foodStr = food.toString();
        String delim = "[ ]+";
        String[] tokens = foodStr.split(delim);
        int foodID = Integer.parseInt(tokens[0]);
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, foodID);
            rs = stmt.executeQuery();
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
        closeConnection();
    }

    //This method pulls all Food Entries from the Database to use for
    //plotting charts/graphs on the 'Analysis' tab of the 'Profile Window'
    public static ArrayList<Food> getAllFoodEntries(){
        ArrayList<Food> foodData = new ArrayList<Food>();
        ResultSet rs = null;
        createConnection();
        try{
            String sql = "SELECT FoodID, Name, FoodDate, Calories, Servings, Favorite FROM FoodEntry "
                + "where PersonID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1,  UfItnesS_GUI_Driver.currentUser.getPersonID());
            rs = stmt.executeQuery();
            for (int row = 0; rs.next(); row++){
                Food foodFromDB = new Food();

                foodFromDB.setFoodID(rs.getInt("FoodID"));
                foodFromDB.setStringFoodDate(rs.getString("FoodDate"));
                foodFromDB.setName(rs.getString("Name"));
                foodFromDB.setCalories(rs.getInt("Calories"));
                foodFromDB.setServings(rs.getInt("Servings"));
                
                foodData.add(foodFromDB);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            closePreparedStatement();
            closeConnection();
        }
        return foodData;
    } 
    
    //This method pulls all Exercise Entries from the Database to use for
    //plotting charts/graphs on the 'Analysis' tab of the 'Profile Window'
    public static ArrayList<Exercise> getAllExerciseEntries(){
        ArrayList<Exercise> exerciseData = new ArrayList<Exercise>();
        ResultSet rs = null;
        createConnection();
        try{
            String sql = "SELECT ExerciseID, Name, ExerciseDate, Calories, Favorite FROM ExerciseEntry "
                + "where PersonID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1,  UfItnesS_GUI_Driver.currentUser.getPersonID());
            rs = stmt.executeQuery();
            for (int row = 0; rs.next(); row++){
                Exercise exerciseFromDB = new Exercise();

                exerciseFromDB.setExerciseID(rs.getInt("ExerciseID"));
                exerciseFromDB.setStringExerciseDate(rs.getString("ExerciseDate"));
                exerciseFromDB.setName(rs.getString("Name"));
                exerciseFromDB.setIntCaloriesBurned(rs.getInt("Calories"));

                exerciseData.add(exerciseFromDB);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            closePreparedStatement();
            closeConnection();
        }
        return exerciseData;
    }    
}
