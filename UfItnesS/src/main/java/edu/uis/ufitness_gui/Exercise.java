package edu.uis.ufitness_gui;

import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 * Created by Kayla on 3/24/2017.
 */
public class Exercise {
    /**
     *
     * @author DC 1075
     */
    private int exerciseID;
    private String name;
    private double mph;
    private int duration;
    private double mets;
    private long caloriesBurned;
    private String  exerciseDate;
    private Boolean exerciseMatch = false;

    public void setExerciseID(int exerciseID) { this.exerciseID = exerciseID; }

    public int getExerciseID() { return exerciseID; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMph() {
        return mph;
    }

    public void setMph(double mph) {
        this.mph = mph;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getMets() {
        return mets;
    }

    public void setMets(int mets) {
        this.mets = mets;
    }

    public void setCaloriesBurned() {
        this.caloriesBurned = calculateCalories();
    }
    
    public void setIntCaloriesBurned(int caloriesInt) {
        this.caloriesBurned = caloriesInt;
    }

    public Long getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setExerciseDate() {
        SimpleDateFormat curFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        this.exerciseDate = curFormatter.format(Calendar.getInstance().getTime());
    }
    
    public void setStringExerciseDate(String dateString){   
       this.exerciseDate = dateString;
    }

    public String getExerciseDate() {
        return exerciseDate;
    }

    public Boolean getExerciseMatch() {
        return exerciseMatch;
    }

    public Exercise addNewExercise(String exerciseName, Double mph, int duration) {
        setName(exerciseName);
        setDuration(duration);
        setMph(mph);
        addExerciseInfo(this);
        setCaloriesBurned();
        setExerciseDate();
        return this;
    }

    public long calculateCalories() {
        int weight = UfItnesS_GUI_Driver.currentUser.getWeight();
        float weightKg = Math.round(weight/2.2);
        double durationInHrs = 0.0167 * duration;
        caloriesBurned = Math.round(mets * weightKg * durationInHrs);
        return caloriesBurned;
    }

    public void addExerciseInfo (Exercise exercise) {
        Exercise[] exerciseArr;
        exerciseArr = new Exercise[38];

        for (int i = 0; i < exerciseArr.length; i++) {
            exerciseArr[i] = new Exercise();
        }
        exerciseArr[0].name = "running";
        exerciseArr[0].mph = 5.0;
        exerciseArr[0].mets = 8.3;

        exerciseArr[1].name = "running";
        exerciseArr[1].mph = 5.5;
        exerciseArr[1].mets = 9.0;

        exerciseArr[2].name = "running";
        exerciseArr[2].mph = 6.0;
        exerciseArr[2].mets = 9.8;

        exerciseArr[3].name = "running";
        exerciseArr[3].mph = 6.5;
        exerciseArr[3].mets = 10.5;

        exerciseArr[4].name = "running";
        exerciseArr[4].mph = 7.0;
        exerciseArr[4].mets = 11.0;

        exerciseArr[5].name = "running";
        exerciseArr[5].mph = 7.5;
        exerciseArr[5].mets = 11.8;

        exerciseArr[6].name = "running";
        exerciseArr[6].mph = 8.0;
        exerciseArr[6].mets = 11.8;

        exerciseArr[7].name = "running";
        exerciseArr[7].mph = 8.5;
        exerciseArr[7].mets = 9.8;

        exerciseArr[8].name = "running";
        exerciseArr[8].mph = 9.0;
        exerciseArr[8].mets = 12.8;

        exerciseArr[9].name = "running";
        exerciseArr[9].mph = 9.5;
        exerciseArr[9].mets = 12.8;

        exerciseArr[10].name = "running";
        exerciseArr[10].mph = 10.0;
        exerciseArr[10].mets = 14.5;

        exerciseArr[11].name = "walking";
        exerciseArr[11].mph = 2.0;
        exerciseArr[11].mets = 2.8;

        exerciseArr[12].name = "walking";
        exerciseArr[12].mph = 2.5;
        exerciseArr[12].mets = 3.0;

        exerciseArr[13].name = "walking";
        exerciseArr[13].mph = 3.0;
        exerciseArr[13].mets = 5.3;

        exerciseArr[14].name = "walking";
        exerciseArr[14].mph = 3.5;
        exerciseArr[14].mets = 4.3;

        exerciseArr[15].name = "walking";
        exerciseArr[15].mph = 4.0;
        exerciseArr[15].mets = 5.0;

        exerciseArr[16].name = "walking";
        exerciseArr[16].mph = 4.5;
        exerciseArr[16].mets = 7.0;

        exerciseArr[17].name = "strength training";
        exerciseArr[17].mets = 6.0;

        exerciseArr[18].name = "calisthenics";
        exerciseArr[18].mets = 3.5;

        exerciseArr[19].name = "swimming";
        exerciseArr[19].mets = 6.0;

        exerciseArr[20].name = "bicycling";
        exerciseArr[20].mph = 10.0;
        exerciseArr[20].mets = 6.8;

        exerciseArr[21].name = "bicycling";
        exerciseArr[21].mph = 11.0;
        exerciseArr[21].mets = 6.8;

        exerciseArr[22].name = "bicycling";
        exerciseArr[22].mph = 12.0;
        exerciseArr[22].mets = 8.0;

        exerciseArr[23].name = "bicycling";
        exerciseArr[23].mph = 13.0;
        exerciseArr[23].mets = 8.0;

        exerciseArr[24].name = "bicycling";
        exerciseArr[24].mph = 14.0;
        exerciseArr[24].mets = 10.0;

        exerciseArr[25].name = "bicycling";
        exerciseArr[25].mph = 15.0;
        exerciseArr[25].mets = 10.0;

        exerciseArr[26].name = "bicycling";
        exerciseArr[26].mph = 16.0;
        exerciseArr[26].mets = 12.0;

        exerciseArr[27].name = "bicycling";
        exerciseArr[27].mph = 17.0;
        exerciseArr[27].mets = 12.0;

        exerciseArr[28].name = "bicycling";
        exerciseArr[28].mph = 18.0;
        exerciseArr[28].mets = 12.0;

        exerciseArr[29].name = "bicycling";
        exerciseArr[29].mph = 19.0;
        exerciseArr[29].mets = 12.0;

        exerciseArr[30].name = "bicycling";
        exerciseArr[30].mph = 19.0;
        exerciseArr[30].mets = 12.0;

        exerciseArr[31].name = "bicycling";
        exerciseArr[31].mph = 20.0;
        exerciseArr[31].mets = 12.0;

        exerciseArr[32].name = "bicycling";
        exerciseArr[32].mph = 21.0;
        exerciseArr[32].mets = 15.8;

        exerciseArr[33].name = "bicycling";
        exerciseArr[33].mph = 22.0;
        exerciseArr[33].mets = 15.8;

        exerciseArr[34].name = "bicycling";
        exerciseArr[34].mph = 23.0;
        exerciseArr[34].mets = 15.8;

        exerciseArr[35].name = "bicycling";
        exerciseArr[35].mph = 24.0;
        exerciseArr[35].mets = 15.8;

        exerciseArr[36].name = "bicycling";
        exerciseArr[36].mph = 25.0;
        exerciseArr[36].mets = 15.8;

        exerciseArr[37].name = "circuit training";
        exerciseArr[37].mets = 8.0;

        for (int i = 0; i < exerciseArr.length; i++) {
            if (this.name.equalsIgnoreCase(exerciseArr[i].name)) {
                if (this.name.equalsIgnoreCase("running")) {
                    for (int j = 0; j < 12; j++) {
                        if (this.mph == exerciseArr[j].mph) {
                            this.mets = exerciseArr[j].mets;
                            exerciseMatch = true;
                        }
                    }
                } else if (this.name.equalsIgnoreCase("walking")) {
                    for (int k = 11; k < 17; k++) {
                        if (this.mph == exerciseArr[k].mph) {
                            this.mets = exerciseArr[k].mets;
                            exerciseMatch = true;
                        }
                    }
                } else if (this.name.equalsIgnoreCase("bicycling")) {
                    for (int m = 20; m < 37; m++) {
                        if (this.mph == exerciseArr[m].mph) {
                            this.mets = exerciseArr[m].mets;
                            exerciseMatch = true;
                        }
                    }
                } else {
                    this.mets = exerciseArr[i].mets;
                    exerciseMatch = true;
                }
            }
        }
    }
}

