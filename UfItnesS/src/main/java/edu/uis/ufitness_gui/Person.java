package edu.uis.ufitness_gui;
import java.awt.image.BufferedImage;
import java.util.Calendar;
import java.util.Date;

public class Person {

	private String name;
	private String userName;
	private String password;
	private int weight; //weight in pounds
	private int height; //height in inches
	private char gender;
	private int age;
	private double BMI;
	private Calendar DOB;
	private BufferedImage image;
	private int personID;
	
	public Person(){
		name = null;
		userName = null;
		password = null;
		weight = 0;
		height = 0;
		gender = 'M';
		age = 0;
		DOB = null;
		BMI = 0;
		image = null;
		personID = 0;
	}
	
	public Person(String name, String userName, String password, int weight, 
			int height, char gender, Calendar DOB, BufferedImage image, int personID){
		
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.DOB = DOB;
		this.age = calcAge(DOB);
		this.BMI = calcBMI(weight, height);
		this.image = image;
		this.personID = personID;
		
				
	}
	
	public void setName(String name){
		this.name = name;
	}	
	public void setUserName(String userName){
		this.userName = userName;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public void setWeight(int weight){
		this.weight = weight;
	}
	public void setHeight(int height){
		this.height = height;
	}
	public void setGender(char gender){
		this.gender = gender;
	}
	public void setAge(int age){
		this.age = age;
	}
	public void setBMI(double BMI){
		this.BMI = BMI;
	}
	public void setImage(BufferedImage image){
		this.image = image;
	}
	public void setPersonID(int personID){
		this.personID = personID;
	}

	public String getName(){
		return name;
	}
	public String getUserName(){
		return userName;
	}
	public String getPassword(){
		return password;
	}
	public int getWeight(){
		return weight;
	}
	public int getHeight(){
		return height;
	}
	public char getGender(){
		return gender;
	}
	public int getAge(){
		return age;
	}
	public double getBMI(){
		return BMI;
	}
	public Calendar getDOB(){
		return DOB;
	}
	public BufferedImage getImage(){
		return image;
	}
	public int getPersonID(){
		return personID;
	}

	//Requirement 1.2.1.1 BMI and Age will be calculated from weight & height,
	// and DOB.
	private double calcBMI(int weightLBS, int heightIN){

		return weightLBS*1.0/(heightIN * heightIN) * 703;
	}
	
	private int calcAge(Calendar DOB){		
		if (Calendar.getInstance().get(Calendar.MONTH) > DOB.get(Calendar.MONTH)){
			age = Calendar.getInstance().get(Calendar.YEAR) - DOB.get(Calendar.YEAR);
		}
		else if (Calendar.getInstance().get(Calendar.MONTH) < DOB.get(Calendar.MONTH)){
			age = Calendar.getInstance().get(Calendar.YEAR) - DOB.get(Calendar.YEAR) - 1;
		}
		else if (Calendar.getInstance().get(Calendar.MONTH) == DOB.get(Calendar.MONTH)){
			if (Calendar.getInstance().get(Calendar.DATE) < DOB.get(Calendar.DATE)){
				age = Calendar.getInstance().get(Calendar.YEAR) - DOB.get(Calendar.YEAR) - 1;
			}
				else age = Calendar.getInstance().get(Calendar.YEAR) - DOB.get(Calendar.YEAR);
			
		}
		return age;
	}

}
