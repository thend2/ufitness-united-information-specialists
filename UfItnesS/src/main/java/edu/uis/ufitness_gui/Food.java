package edu.uis.ufitness_gui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.DefaultListModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Macon
 */
public class Food {
    
    private int foodID;
    private int personID;
    private String foodDate;
    private String name;
    private long calories;
    private boolean favorite;
    private int servings;
    private ArrayList<String> currentFood = new ArrayList<>();
    private ArrayList<String> recentFood = new ArrayList<>();
    private ArrayList<String> favoriteFood = new ArrayList<>();
    DefaultListModel<String> current = new DefaultListModel<>();
    DefaultListModel<String> recent = new DefaultListModel<>();
    DefaultListModel<String> favoriteL = new DefaultListModel<>();

    public int getFoodID(){
        return foodID;
    }
    public void setFoodID(int foodID){
        this.foodID = foodID;
    }
    public int getPersonID(){
        return personID;
    }
    public void setPersonID(int personID){
        this.personID = personID;
    }
    public String getFoodDate(){
        return foodDate;
    }
    public void setFoodDate(){
        SimpleDateFormat curFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        this.foodDate = curFormatter.format(Calendar.getInstance().getTime());
    }
    
    public void setStringFoodDate(String dateString){ 
        this.foodDate = dateString;
    }
    
//    public String getFormatDate(){
//        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
//        String formatedDate = format.format(foodDate);
//        return formatedDate;
//    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public long getCalories(){
        return calories;
    }
    public void setCalories(long calories){
        this.calories = calories;
    }
    public boolean getFavorite(){
        return favorite;
    }
    public void setFavorite(boolean favorite){
        this.favorite = favorite;
    }
    public int getServings(){
        return servings;
    }
    public void setServings(int servings){
        this.servings = servings;
    }
    public ArrayList<String> getCurrentFood(){
        return currentFood;
    }
    public void setCurrentFood(ArrayList<String> currentFood){
        this.currentFood = currentFood;
    }
    public ArrayList<String> getRecentFood(){
        return recentFood;
    }
    public void setRecentFood(ArrayList<String> recentFood){
        this.recentFood = recentFood;
    }
    public ArrayList<String> getFavoriteFood(){
        return favoriteFood;
    }
    public void setFavoriteFood(ArrayList<String> favoriteFood){
        this.favoriteFood = favoriteFood;
    }
    public void addCurrentFood(String foodName){
        //currentFood.add(foodName);
        currentFood.add(foodName);
    }
    public void addRecentFood(String foodName){
        recentFood.add(foodName);
    }
    public void addFavoriteFood(String foodName){
        favoriteFood.add(foodName);
    }
    public long totalCalories(){
        return (long) Math.round(calories * servings);
    }
    public DefaultListModel<String> getCurrent(){
        return current;
    }
    public void setCurrent(DefaultListModel<String> current){
        this.current = current;
    }
    public void addCurrent(String foodName){
        current.addElement(foodName);
    }
}
