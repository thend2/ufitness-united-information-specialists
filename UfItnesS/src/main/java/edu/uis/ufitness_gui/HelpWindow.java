package edu.uis.ufitness_gui;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class HelpWindow extends JFrame {
	private JEditorPane helpPane;
	private JScrollPane scrollPane;
	
	public HelpWindow() {
		initComponents();
	}
	
	private void initComponents() {
		helpPane = new JEditorPane();
		helpPane.setEditable(false);
		try {
			helpPane.setPage(getClass().getResource("User Manual.html"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		scrollPane = new JScrollPane(helpPane);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		this.add(scrollPane);
		
		this.setIconImage(new ImageIcon(
				getClass().getResource("fit_icon.png")).getImage());
		this.setSize(new Dimension(600, 600));
		this.setTitle("UfItnesS User Manual");
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
	
}
